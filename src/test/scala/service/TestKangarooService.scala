package com.dsncode
package service

import org.scalatest.funsuite.AnyFunSuite

class TestKangarooService extends AnyFunSuite {

  test("Kangaroo Meet") {
    val k1 = 0;
    val v1 = 3;
    val k2 = 4;
    val v2 = 2;
    val expectation = "YES"
    val response = KangarooService.kangaroo(k1, v1, k2, v2)
    assert(expectation == response)
  }

  test("Kangaroo 1 pass Kangaroo 2") {
    val k1 = 0;
    val v1 = 2;
    val k2 = 5;
    val v2 = 3;
    val expectation = "NO"
    val response = KangarooService.kangaroo(k1, v1, k2, v2)
    assert(expectation == response)
  }

  test("long run. kangaroo meet"){
    val k1 = 43;
    val v1 = 2;
    val k2 = 70;
    val v2 = 2;
    val expectation = "NO"
    val response = KangarooService.kangaroo(k1, v1, k2, v2)
    assert(expectation == response)


  }

}
