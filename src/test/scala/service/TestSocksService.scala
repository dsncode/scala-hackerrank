package com.dsncode
package service

import org.scalatest.funsuite.AnyFunSuite

class TestSocksService extends AnyFunSuite {

  test("found 2 pairs") {
    val sample = Array(1, 2, 1, 2, 1, 3, 2)
    val expected = 2
    val pairs = SocksService.sockMerchant(sample.length, sample)
    assert(pairs == expected)
  }

  test("found no pairs"){
    val sample = Array(1,2,3,4,5,6)
    val expected = 0
    val pairs = SocksService.sockMerchant(sample.length, sample)
    assert(pairs == expected)
  }

}
