package com.dsncode
package service

import org.scalatest.funsuite.AnyFunSuite

class TestLinkedListService extends AnyFunSuite {


  def createNodeNetwork(maxNodes: Int): Node = {
    var lastNode = new Node(Option.empty, maxNodes)
    val remainingNodesToBuild = maxNodes - 1
    for (refValue <- remainingNodesToBuild to(1, -1)) {
      lastNode = new Node(Option(lastNode), refValue)
    }
    lastNode
  }

  test("big number of EVEN nodes") {
    val maxNodes = 10000
    val referenceNode = createNodeNetwork(maxNodes)
    val centerNode = LinkedListService.findCenterNode(referenceNode)

    val expectedCenterNode = maxNodes / 2 + 1

    assert(centerNode.isDefined)
    assert(centerNode.get.value == expectedCenterNode)
  }

  test("big number of ODD nodes") {
    val maxNodes = 10001
    val referenceNode = createNodeNetwork(maxNodes)
    val centerNode = LinkedListService.findCenterNode(referenceNode)
    val expectedCenterNode = maxNodes / 2 + 1

    assert(centerNode.isDefined)
    assert(centerNode.get.value ==expectedCenterNode)
  }

  test("find center node on an ODD number of nodes") {
    val n5 = new Node(Option.empty, 5)
    val n4 = new Node(Option(n5), 4)
    val n3 = new Node(Option(n4), 3)
    val n2 = new Node(Option(n3), 2)
    val n1 = new Node(Option(n2), 1)

    val centerNode = LinkedListService.findCenterNode(n1)
    assert(centerNode.isDefined)
    assert(centerNode.get.value == n3.value)
  }

  test("find center node on an EVEN number of nodes") {
    val n6 = new Node(Option.empty, 6)
    val n5 = new Node(Option(n6), 5)
    val n4 = new Node(Option(n5), 4)
    val n3 = new Node(Option(n4), 3)
    val n2 = new Node(Option(n3), 2)
    val n1 = new Node(Option(n2), 1)

    val centerNode = LinkedListService.findCenterNode(n1)
    assert(centerNode.isDefined)
    assert(centerNode.get.value == n4.value)
  }

}
