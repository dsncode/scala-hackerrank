package com.dsncode
package service

import org.scalatest.funsuite.AnyFunSuite

class TestChocoService extends AnyFunSuite{

  test("9 chocolate bars"){
    val money = 15
    val cost = 3
    val exchange = 2

    val expected = 9
    val response = ChocoService.chocolateFeast(money, cost, exchange)

    assert(expected == response)

  }



}
