package com.dsncode
package service

import scala.annotation.tailrec

// TEST: Given a single-linked list, find the center node of the linked list:
// If there is an even number of nodes, then print node number “array.length/2+1”:

class Node(val nextNode : Option[Node], val value: Int)

object LinkedListService {

  def findCenterNode(node: Node) : Option[Node] = {

    @tailrec
    def countNodes(refNode: Option[Node], count:Int): Int ={
      refNode match{
        case None => count
        case Some(existNode) => countNodes(existNode.nextNode, count + 1)
      }
    }

    @tailrec
    def findNode(refNode:Option[Node], currentPosition: Int, targetPosition: Int) : Option[Node] = {
      if(currentPosition == targetPosition){
        refNode
      }else{
        findNode(refNode.get.nextNode, currentPosition + 1, targetPosition)
      }
    }

    val startOptionalNode = Option(node)
    val totalNodes = countNodes(startOptionalNode, 0)

    val stopNode = totalNodes / 2 + 1

    findNode(startOptionalNode, 1, stopNode)

  }



}
