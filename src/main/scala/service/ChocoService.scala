package com.dsncode
package service

import scala.annotation.tailrec

// Test: https://www.hackerrank.com/challenges/chocolate-feast/problem
object ChocoService {

  def chocolateFeast(money: Int, cost: Int, exchange: Int): Int = {

    @tailrec
    def computeFest(wrappers: Int, accChocolate: Int): Int = {

      val toEatChocolate = wrappers / exchange
      val remainingWrappers = (wrappers % exchange) + toEatChocolate

      if (remainingWrappers >= exchange) {
        computeFest(remainingWrappers, accChocolate + toEatChocolate)
      } else {
        accChocolate + toEatChocolate
      }
    }


    val wrappersAfterPurchase = money / cost
    val eatenChocolate = wrappersAfterPurchase

    computeFest(wrappersAfterPurchase, eatenChocolate)
  }
}
