package com.dsncode
package service

import scala.annotation.tailrec
// TEST: https://www.hackerrank.com/challenges/kangaroo/problem
object KangarooService {
  def kangaroo(x1: Int, v1: Int, x2: Int, v2: Int): String = {
    // Write your code here

    @tailrec
    def iter(p1: Int, p2: Int, lastPosDiff: Int): String = {

      val currentPosDiff = p1 - p2
      println(s"k1:$p1 - k2: $p2 => diff: $currentPosDiff")
      if (currentPosDiff == 0) {
        println("Meet!")
        "YES"
      } else {
        val isDecreasingDistance = Math.abs(currentPosDiff) < Math.abs(lastPosDiff)
        if(isDecreasingDistance) {
          iter(p1 + v1, p2 + v2, currentPosDiff)
        } else {
          "NO"
        }
      }
    }
    iter(x1, x2, Int.MaxValue)
  }

}
