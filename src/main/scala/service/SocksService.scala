package com.dsncode
package service

import scala.annotation.tailrec
import scala.collection.immutable.HashMap
// TEST: https://www.hackerrank.com/challenges/sock-merchant/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=warmup
object SocksService {

  @tailrec
  def iter(arr: List[Int], acc: HashMap[Int, Int], pairs: Int): Int = {
    arr match {
      case Nil => pairs
      case color :: tail => {
        val currentCount = acc.get(color) match {
          case None => 1
          case Some(i) => i + 1
        }
        val newMap: HashMap[Int, Int] = acc + (color -> currentCount)
        val foundPair = currentCount % 2 match {
          case 0 => 1
          case 1 => 0
        }
        iter(tail, newMap, pairs + foundPair)
      }
    }
  }

  def sockMerchant(n: Int, ar: Array[Int]): Int = iter(ar.toList, new HashMap[Int, Int](), 0)


}
