name := "scala-tests"

version := "0.1"

scalaVersion := "2.13.6"

idePackagePrefix := Some("com.dsncode")
libraryDependencies += "org.scalatest" %% "scalatest-funsuite" % "3.2.9" % "test"
